<?php

namespace App\Repository;

use App\Entity\Lemmed;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Lemmed|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lemmed|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lemmed[]    findAll()
 * @method Lemmed[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LemmedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lemmed::class);
    }

    public function findOneByNameField($value): ?Lemmed
    {
        $value = json_encode(count_chars($value, 1));
        return $this->createQueryBuilder('l')
            ->andWhere('l.character_array = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }
}
