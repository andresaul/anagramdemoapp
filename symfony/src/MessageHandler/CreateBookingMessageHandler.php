<?php

namespace App\MessageHandler;

use App\Entity\Lemmed;
use App\Message\CreateBookingMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;


class CreateBookingMessageHandler implements MessageHandlerInterface
{
    private $entityManager;
    private $appKernel;

    public function __construct(
        EntityManagerInterface $entityManager,
        KernelInterface $appKernel
    ) {
        $this->entityManager = $entityManager;
        $this->appKernel = $appKernel;
    }

    public function __invoke(CreateBookingMessage $bookingMessage)
    {
        $connection = $this->entityManager->getConnection();
        $platform   = $connection->getDatabasePlatform();
        $connection->executeUpdate($platform->getTruncateTableSQL('lemmed', true));
        $filePath = $this->appKernel->getProjectDir() . '/web/lemmad2013.txt';

        $lemmedFile = file($filePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $i = 0;
        foreach ($lemmedFile as $lemme) {
            $i++;
            if (empty($lemme)) {
                continue;
            }

//            if ( $i > 500){
//                break;
//            }

            $user = new Lemmed();
            $user->setName($lemme);
            $this->entityManager->persist($user);

            if ($i % 1000 == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
//                dump($i);
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();

    }
}
