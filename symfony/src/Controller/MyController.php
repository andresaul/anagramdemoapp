<?php

namespace App\Controller;

use App\Message\CreateBookingMessage;
use App\Repository\LemmedRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;


 class MyController extends AbstractController
{
    /**
     * @var LemmedRepository
     */
     private $lemmedRepository;

     public function __construct(
         LemmedRepository $lemmedRepository
     ) {
         $this->lemmedRepository = $lemmedRepository;
     }

    /**
     * @Route("/is-anagram/{word}", name="isAnagram")
     */
    public function isAnagram($word)
    {
        $lemme = $this->lemmedRepository->findOneByNameField($word);
        $response = new Response();
        $date = new \DateTime();

        $response->setContent(json_encode([
            'time'    => $date->format("Y-m-d H:i:s"),
            'status'  => $lemme ? 1 : 0,
            'anagram' => $lemme ? $lemme->getName() : '',
        ]));

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/leemed-count", name="getLemmedCount")
     */
    public function getLemmedCount()
    {
        $lemme = $this->lemmedRepository->count([]);
        $response = new Response();
        $date = new \DateTime();

        $response->setContent(json_encode([
            'time'    => $date->format("Y-m-d H:i:s"),
            'count'  => $lemme,
        ]));

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/import-lemmed", name="importLemmed")
     */
    public function importLemmed(MessageBusInterface $messageBus)
    {
        $messageBus->dispatch(new CreateBookingMessage());
        $response = new Response();
        $date = new \DateTime();

        $response->setContent(json_encode([
            'id'      => uniqid(),
            'message' => '-*-',
            'time'    => $date->format("Y-m-d"),
        ]));

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }
}
