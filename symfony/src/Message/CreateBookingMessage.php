<?php

namespace App\Message;

class CreateBookingMessage
{
    private $name;

    public function __construct($name = null)
    {
        $this->name = $name;
    }
    public function getName(): string
    {
        return $this->name;
    }
}
