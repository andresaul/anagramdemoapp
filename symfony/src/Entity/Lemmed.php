<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LemmedRepository")
 */
class Lemmed
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="json")
     */
    private $character_array;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        $this->setCharacterArray($name);

        return $this;
    }

    public function getCharacterArray(): ?array
    {
        return $this->character_array;
    }

    public function setCharacterArray(string $name): self
    {
        $this->character_array = json_encode(count_chars($name, 1) );

        return $this;
    }
}
