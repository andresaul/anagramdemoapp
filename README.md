# Demo app
Symfony 4 restful API demonstration. 

This project demonstrates how to handle a huge text file import and process with Symfony background process. 

Backend endpoints 
* @Route("/is-anagram/{word}", name="isAnagram")
* @Route("/leemed-count", name="getLemmedCount")
* @Route("/import-lemmed", name="importLemmed")

For frontend is a simple Vue.js  app http://localhost/

### Prerequisites
* [Docker](https://www.docker.com/)

### Installing

run docker and connect to container:
```
 docker-compose up --build
```
access to symfony console
```
 docker-compose exec php sh
```
create file .env
Example:
```
APP_ENV=dev
DATABASE_URL=mysql://root:root@mysql:3306/symfony
```
install latest version of [Symfony](http://symfony.com/doc/current/setup.html) via composer:
```
composer install
```
