import Vue from 'vue'
import Buefy from 'buefy'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import 'buefy/dist/buefy.css'
import 'bulma/css/bulma.css'

import App from '@/App.vue'

Vue.use(Buefy, {
  defaultIconPack: 'fas'
})

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
